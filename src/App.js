import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';

import { Header } from './components/header';
import { Main } from './pages/main';
import { MovieInfo } from './pages/movieInfo';

export default function App({ store }) {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Header />
                <Switch>
                    <Route component={Main} path="/" exact></Route>
                    <Route component={MovieInfo} path="/movieInfo/:id"></Route>
                </Switch>
            </BrowserRouter>
        </Provider>
    );
}
