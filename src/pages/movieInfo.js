import React from 'react';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    root: {
        display: 'flex',
        marginTop: 20,
        paddingLeft: 100,
        paddingRight: 100,
    },
    img: {
        width: '25%',
    },
    textwrap: {
        marginLeft: 30,
    },
});
export const MovieInfo = () => {
    const classes = useStyles();
    const params = useParams();
    const movie = useSelector((state) => state.movies.find((movie) => movie.char_id === Number(params.id)));
    return (
        <>
            <div className={classes.root}>
                <img className={classes.img} src={movie.img} alt={movie.name}></img>
                <div className={classes.textwrap}>
                    <p>name: {movie.name}</p>
                    <p>birthday: {movie.birthday}</p>
                    <p>nickname: {movie.nickname}</p>
                    <p>portrayed: {movie.portrayed}</p>
                </div>
            </div>
        </>
    );
};
