import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Search } from './../components/search';
import { Filter } from './../components/filter';
import { MovieList } from './../components/movies';
import { asyncGetMovie, searchMovie, filterMovie } from '../actions/actions';

export const Main = () => {
    const dispatch = useDispatch();
    const movies = useSelector((state) =>
        state.movies.filter((movie) => movie.name.toLowerCase().includes(state.searchMovies.toLowerCase())),
    );

    useEffect(() => {
        dispatch(asyncGetMovie());
    }, []);

    return (
        <>
            <Search search={(name) => dispatch(searchMovie(name))} />
            <Filter list={movies} filter={(filter) => dispatch(filterMovie(filter))} />
            <MovieList list={movies} />
        </>
    );
};
