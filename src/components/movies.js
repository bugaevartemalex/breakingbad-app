import React from 'react';
import { NavLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles(() => ({
    wrapper: {
        paddingLeft: 100,
        paddingRight: 100,
        marginTop: 20,
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        margin: -20,
    },
    li: {
        listStyle: 'none',
        margin: 20,
    },
    link: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        textDecoration: 'none',
        color: '#3f51b5',
    },
    big: {
        width: 80,
        height: 80,
    },
    spinner: {
        position: 'absolute',
        left: '50%',
        top: '50%',
        transform: 'translate(-50%,-50%)',
    },
}));

export const MovieList = ({ list }) => {
    const classes = useStyles();
    if (!list.length) {
        return (
            <>
                <div className={classes.spinner}>
                    <CircularProgress />
                </div>
            </>
        );
    }
    return (
        <>
            <div className={classes.wrapper}>
                <ul className={classes.root}>
                    {list.map((movie, i) => (
                        <li className={classes.li} key={i}>
                            <NavLink to={`/movieInfo/${movie.char_id}`} className={classes.link}>
                                <Avatar alt="{movie.name}" src={movie.img} className={classes.big} />
                                <span>{movie.name}</span>
                            </NavLink>
                        </li>
                    ))}
                </ul>
            </div>
        </>
    );
};
