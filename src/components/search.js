import React, { useState, useEffect } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        paddingLeft: 100,
        paddingRight: 100,
        marginTop: 20,
    },
    searchButton: {
        width: 'calc(25% - 30px)',
        marginLeft: 30,
    },
    search: {
        width: '75%',
        flexGrow: 1,
    },
}));

export const Search = ({ search }) => {
    const classes = useStyles();
    const [input, setInput] = useState('');
    const searchMovie = () => {
        search(input);
    };
    useEffect(() => {
        const inputText = JSON.parse(localStorage.getItem('input') || '');
        setInput(inputText);
        console.log(inputText);
    }, []);
    useEffect(() => {
        localStorage.setItem('input', JSON.stringify(input));
    }, [input]);
    return (
        <>
            <form className={classes.root} noValidate autoComplete="off">
                <TextField
                    className={classes.search}
                    value={input}
                    onChange={(e) => {
                        setInput(e.target.value);
                    }}
                    id="standard-basic"
                    label="Standard"
                />
                <Button className={classes.searchButton} onClick={searchMovie} variant="outlined" color="primary">
                    Search
                </Button>
            </form>
        </>
    );
};
