import React from 'react';
import { AppBar, Toolbar, Typography } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

export const Header = () => {
    const history = useHistory();
    return (
        <>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" onClick={() => history.push('/')}>
                        Breaking Bad App
                    </Typography>
                </Toolbar>
            </AppBar>
        </>
    );
};
