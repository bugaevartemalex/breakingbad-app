import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles(() => ({
    wrapper: {
        display: 'flex',
        justifyContent: 'space-between',
        marginTop: 20,
        paddingLeft: 100,
        paddingRight: 100,
    },
    root: {
        marginRight: 30,
    },
    formControl: {
        width: 150,
        marginLeft: 20,
    },
}));

export const Filter = ({ list, filter }) => {
    const classes = useStyles();
    const [sort, setSort] = useState('char_id');
    const sortSelect = () => {
        list.sort((a, b) => {
            return a[sort].toString().localeCompare(b[sort]);
        });
        return filter(list);
    };

    useEffect(() => {
        sortSelect();
    }, [sort]);

    const handleChange = (event) => {
        setSort(event.target.value);
    };

    const Sorted = (sortType) => {
        list.sort((a, b) => {
            const isReversed = sortType === 'asc' ? 1 : -1;
            return isReversed * a[sort].toString().localeCompare(b[sort]);
        });
        return filter(list);
    };

    return (
        <>
            <div className={classes.wrapper}>
                <ButtonGroup className={classes.root} color="primary" aria-label="outlined primary button group">
                    <Button value="alignment" onClick={() => Sorted('asc')}>
                        Ascending
                    </Button>
                    <Button onClick={() => Sorted('', 'desc')}>Descending</Button>
                </ButtonGroup>
                <div>
                    <FormControl className={classes.formControl}>
                        <InputLabel id="demo-simple-select-helper-label">sort by</InputLabel>
                        <Select
                            labelId="demo-simple-select-helper-label"
                            id="demo-simple-select-helper"
                            value={sort}
                            onChange={handleChange}
                        >
                            <MenuItem value={'char_id'}>Id</MenuItem>
                            <MenuItem value={'name'}>Name</MenuItem>
                        </Select>
                    </FormControl>
                </div>
            </div>
        </>
    );
};
