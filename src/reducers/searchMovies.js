const initialState = '';

export default function searchMovies(state = initialState, action) {
    switch (action.type) {
        case 'SEARCH_MOVIE':
            return action.payload;
        default:
            return state;
    }
}
