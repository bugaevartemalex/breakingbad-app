const initialState = [];

export default function movies(state = initialState, action) {
    switch (action.type) {
        case 'FETCH_MOVIE':
            return action.payload;
        case 'FILTER_MOVIE':
            return action.payload; // это наверно не правильно, тк мы нарушаем иммутабельность
        default:
            return state;
    }
}
