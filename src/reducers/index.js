import { combineReducers } from 'redux';
import movies from './movies';
import searchMovies from './searchMovies';

export const rootReducer = combineReducers({
    movies,
    searchMovies,
});
