export const asyncGetMovie = () => async (dispatch) => {
    const response = await fetch('https://breakingbadapi.com/api/characters').then((response) => response.json());
    dispatch({ type: 'FETCH_MOVIE', payload: response });
};

export const searchMovie = (name) => {
    return { type: 'SEARCH_MOVIE', payload: name };
};

export const filterMovie = (fiter) => {
    return { type: 'FILTER_MOVIE', payload: fiter };
};
